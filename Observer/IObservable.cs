﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    //Tambien llamada clase interfaz sujeto
    interface IObservable
    {
        //Propiedades
        void Agrega(IObservador ob);
        void Elimina(IObservador ob);
        //Metodos
        void Notifica();
    }
}

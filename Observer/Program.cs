﻿using System;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            //creamos al sujeto solido(Sensor)
            Sensor_Movimiento sensor = new Sensor_Movimiento();
            //Creamos los usuarios(Observadores solidos) 
            Observador usuario1 = new Observador("Jose", sensor);
            Observador usuario2 = new Observador("Maria", sensor);
            Observador usuario3 = new Observador("Paco", sensor);
            //Procedemos a hacer la simulacion de movimiento

            for (int i = 0; i < 5; i++)
            {
                sensor.Movimiento();
            }

            //Si Alguno se quiere Salir
            Console.WriteLine("Elimando Usuario");
            sensor.Elimina(usuario2);

            for (int i = 0; i < 5; i++)
            {
                sensor.Movimiento();
            }
        }
    }
}

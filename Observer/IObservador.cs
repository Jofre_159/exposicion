﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    //Clase Interfaz Observador
    interface IObservador
    {
        //Metodos
            //Modelo Push
        void Actualiza(String Mensaje);
            //Modelo Pull
        void ActualizaPull();

    }
}

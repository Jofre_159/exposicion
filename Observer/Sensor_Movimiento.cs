﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    //Tambien llamada clase sujeto concreto
    class Sensor_Movimiento : IObservable

    {
        //validamos y creamos una lista para guardar a los observadores a la cual llamaremos Usuarios
        IList<IObservador> Usuarios = new List<IObservador>();
        String mensaje;
        //Creamos una variable randon para una simulacion de un proceso.
        private Random random = new Random();
        int n;
        public int N { get => n; set => n = value; }
        public void Agrega(IObservador ob)
        {
            //Añadimos un observador a la lista de los Usuarios observadores
            Usuarios.Add(ob);
           
        }

        public void Elimina(IObservador ob)
        {
            //Quitamos un observador de la lista de los Usuarios observadores
            Usuarios.Remove(ob);
        }

        public void Notifica()
        {
            //Notificamos el nuevo estado a todos los observadores
            foreach (IObservador ob in Usuarios) {
                ob.Actualiza(mensaje);
                //ob.ActualizaPull();
            }
                
        }
        public void Movimiento()
        {
            //simulamos un movimiento en el sensor con una variable randon para cambiar el estado de este sujeto.
            n = random.Next(20);
            if (n % 5 == 0) 
            {
                Console.WriteLine("Movimiento detectado");
                mensaje = ("El movimiento se registro en:"+n);
                Notifica();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    //Clase Observador Concreto
    class Observador : IObservador
    {
        //variable para el constructor del observador
        String nombre;
        //variable referencia al sujeto solido que es de la clase sensor movimiento
        Sensor_Movimiento sujeto;
        //constructor para Observador con parametros nombre del usuario y sujeto que en este caso seria el sensor
        public Observador(String Pnombre, Sensor_Movimiento Psujeto) {
            //asignamos valores
            nombre = Pnombre;
            sujeto = Psujeto;
            //Agregamos a la lista de usuarios que van a observar cualquier movimiento en el sensor
            sujeto.Agrega(this);
        }
        //modelo Push: Nos notifica de los cambios con toda la informacion
        public void Actualiza(string Mensaje)
        {
            Console.WriteLine("Modelo Push:{0}-{1}",nombre,Mensaje);
        }
        //Modelo Pull: notifica de los cambios pero solo si el usuario quiere se otorga la informacion de este
        public void ActualizaPull()
        {
            //aqui con cierta logica, para preguntar al usuario si quiere saber la informacion
            int n = sujeto.N;
            Console.WriteLine("Modelo Pull:{0}-{1}", nombre, n);
        }
    }
}
